let value1 = document.getElementById("input1");
let value2 = document.getElementById("input2");
let formHasil = document.getElementById("hasil");

function tambah() {
    let hasilTambah = parseInt(value1.value) + parseInt(value2.value);
    formHasil.value = hasilTambah;
}

function kurang() {
    let hasilKurang = parseInt(value1.value) - parseInt(value2.value);
    formHasil.value = hasilKurang;
}

function kali() {
    let hasilKali = parseInt(value1.value) * parseInt(value2.value);
    formHasil.value = hasilKali;
}

function bagi() {
    let hasilBagi = parseInt(value1.value) / parseInt(value2.value);
    formHasil.value = hasilBagi;
}

function akar() {
    const formInput = document.getElementById("inputValue");
    let inputAkar = document.getElementById("inputAkar");

    formInput.innerHTML = `<input type="number" id="inputAkar" placeholder="Masukan angka" style="width: 185px;"> <br><br>`;
    if (inputAkar.value != "") {
        formHasil.value = Math.sqrt(parseInt(inputAkar.value));
    }
}

function pangkat() {
    let hasilPangkat = Math.pow(parseInt(value1.value), parseInt(value2.value));
    formHasil.value = hasilPangkat;
}

function mod() {
    let hasilPangkat = parseInt(value1.value) % parseInt(value1.value);
    formHasil.value = hasilPangkat;
}

function sin() {
    const formInput = document.getElementById("inputValue");
    let inputAkar = document.getElementById("inputAkar");

    formInput.innerHTML = `<input type="number" id="inputAkar" placeholder="Masukan angka" style="width: 185px;"> <br><br>`;
    if (inputAkar.value != "") {
        formHasil.value = Math.sin(parseInt(inputAkar.value));
    }
}

function cos() {
    const formInput = document.getElementById("inputValue");
    let inputAkar = document.getElementById("inputAkar");

    formInput.innerHTML = `<input type="number" id="inputAkar" placeholder="Masukan angka" style="width: 185px;"> <br><br>`;
    if (inputAkar.value != "") {
        formHasil.value = Math.cos(parseInt(inputAkar.value));
    }
}

function tan() {
    const formInput = document.getElementById("inputValue");
    let inputAkar = document.getElementById("inputAkar");

    formInput.innerHTML = `<input type="number" id="inputAkar" placeholder="Masukan angka" style="width: 185px;"> <br><br>`;
    if (inputAkar.value != "") {
        formHasil.value = Math.tan(parseInt(inputAkar.value));
    }
}

function reset() {
    location.reload();
}